import { render, screen } from "@testing-library/react";
import { App } from "./App";

test("check first h1 text", () => {
  render(<App />);
  const linkElement = screen.getByText(/Hello World/i);
  expect(linkElement).toBeInTheDocument();
});
