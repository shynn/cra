module.exports = {
  testEnvironment: "jsdom",
  setupFilesAfterEnv: ["<rootDir>/src/tests/setupTests.js"],
  moduleDirectories: ["node_modules", "src"],
  moduleNameMapper: {
    "^.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$":
      "jest-transform-stub",
    "^~root(.*)$": "<rootDir>/src$1",
    "^~core(.*)$": "<rootDir>/src/core$1",
    "^~configs(.*)$": "<rootDir>/src/configs$1",
    "^~layouts(.*)$": "<rootDir>/src/layouts$1",
    "^~redux(.*)$": "<rootDir>/src/redux$1",
    "^~services(.*)$": "<rootDir>/src/services$1",
    "^~utils(.*)$": "<rootDir>/src/utils$1",
    "^~test-utils": "<rootDir>/src/tests/utils.js",
    "^~routes": "<rootDir>/src/routes.jsx",
  },
  transform: {
    ".+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$":
      "jest-transform-stub",
    "^.+\\.(js|jsx)$": "babel-jest",
  },
};
