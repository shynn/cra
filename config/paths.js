const { resolve } = require("path");
const { realpathSync } = require("fs");

const appDirectory = realpathSync(process.cwd());
const resolveApp = (relativePath) => resolve(appDirectory, relativePath);

const alias = {
  "~root": resolveApp("src"),
  "~core": resolveApp("src/core"),
  "~configs": resolveApp("src/configs"),
  "~layouts": resolveApp("src/layouts"),
  "~redux": resolveApp("src/redux"),
  "~services": resolveApp("src/services"),
  "~utils": resolveApp("src/utils"),
  "~test-utils": resolveApp("src/tests/utils.tsx"),
  "~routes": resolveApp("src/routes.tsx"),
};

module.exports = {
  entry: resolveApp("src/index.jsx"),
  src: resolveApp("src"),
  build: resolveApp("dist"),
  public: resolveApp("public"),
  assets: resolveApp("src/assets"),
  alias,
};
