const { build } = require("./paths");
const webpackCommon = require("./webpack.common");

const { merge } = require("webpack-merge");
const Dotenv = require("dotenv-webpack");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");

module.exports = merge(webpackCommon, {
  mode: "development",
  devtool: "inline-source-map",
  devServer: {
    historyApiFallback: true,
    contentBase: build,
    open: false,
    compress: true,
    hot: true,
    port: 3000,
  },
  module: {
    rules: [
      // ... other loaders
      {
        test: /\.[js]sx?$/,
        exclude: /node_modules/,
        use: [
          // ... other options
          {
            loader: require.resolve("babel-loader"),
            options: {
              plugins: [
                // ... other plugins
                require.resolve("react-refresh/babel"),
              ].filter(Boolean),
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new Dotenv({
      path: "./.env.development",
    }),
    new ESLintPlugin(),
    new ReactRefreshWebpackPlugin(),
  ].filter(Boolean),
});
