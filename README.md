# Shynn CRA - Typescript Template

- A create-react-app template created by [Shynn](https://fb.com/shynnhuy)

## Built with

- [React 17](https://reactjs.org/) - A JavaScript library for building user interfaces.
- [TypeScript](https://www.typescriptlang.org/) - Extends JavaScript by adding types.
- [Webpack 5](https://webpack.js.org/) - A static module bundler for JavaScript applications.
- [React Router Dom v6](https://github.com/ReactTraining/react-router/tree/dev) - A lightweight, fully-featured routing library for the React JavaScript library
- [ESLint](https://eslint.org/) - An analysis tool for identifying problematic patterns found in JavaScript code.
- [Jest](https://jestjs.io/) - A delightful JavaScript Testing Framework with a focus on simplicity.
- [React Testing Library](https://testing-library.com/docs/react-testing-library/intro) - A family of packages helps you test UI components in a user-centric way.
- [Husky](https://typicode.github.io/husky/#/) - Modern native git hooks.
- [Lint-staged](https://github.com/okonet/lint-staged) - Run linters against staged git files.
- [Commitlint](https://commitlint.js.org/#/) - Helps your team adhering to a commit convention.

## Commitlint Convention

```php
type(scope?): subject
```

### type

- build: Changes that affect the build system or external dependencies (example scopes: gulp, broccoli, npm)
- ci: Changes to our CI configuration files and scripts (example scopes: Gitlab CI, Circle, BrowserStack, SauceLabs)
- chore: add something without touching production code (Eg: update npm dependencies)
- docs: Documentation only changes
- feat: A new feature
- fix: A bug fix
- perf: A code change that improves performance
- refactor: A code change that neither fixes a bug nor adds a feature
- revert: Reverts a previous commit
- style: Changes that do not affect the meaning of the code (Eg: adding white-space, formatting, missing semi-colons, etc)
- test: Adding missing tests or correcting existing tests

### scope - optional

### subject - content of the commit

## Author

- [Shynn Huy](https://fb.com/shynnhuy)

## License

[MIT](https://choosealicense.com/licenses/mit/)
